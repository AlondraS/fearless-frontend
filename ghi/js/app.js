function createCard(name, description, pictureUrl, starts, ends, location) {
  const startDate = new Date(starts).toLocaleDateString();
  const endDate = new Date(ends).toLocaleDateString();
    return `
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-subtitle mb-2 text-muted">${location}</p>
          <p class="card-text">${description}</p>
          <footer class="card-footer">${startDate} - ${endDate}</footer>
        </div>
      </div>
    `;
  }


function errorMessage() {
  return `
  <div class="alert alert-info" role="alert">
  A simple info alert—check it out!
  </div>`
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
    
        if (!response.ok) {
            throw new Error('Error: Response not OK')
            
        } else {
            const data = await response.json();
           

          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const starts = details.conference.starts;
              const ends = details.conference.ends;
              const pictureUrl = details.conference.location.picture_url;
              const location = details.conference.location.name;
              const html = createCard(title, description, pictureUrl, starts, ends, location);
              const column = document.querySelector('.row-cols-3');
              column.innerHTML += html;
            }
          }
    
        }
      } catch (e) {
        console.error('Error:', e);
      }

});
